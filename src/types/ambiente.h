#ifndef AMBIENTE
#define AMBIENTE

#include <stdio.h>
#include <stdlib.h>
#include "types.h"

typedef struct ambiente {
  string_t x, y, z, params[3], currentDir, fileNameIn, fileNameOut, fileExt;
  FILE *arqIn, *arqOut[10];
  int linhaAtual;
} ambiente_t;

ambiente_t *newAmbiente();

void freeAmbiente(ambiente_t **a);

#endif