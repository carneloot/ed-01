#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "args.h"
#include "comandos.h"
#include "files/files.h"
#include "types/types.h"

int main(int argc, string_t argv[]) {
  ambiente_t *ambiente = newAmbiente();

  handleArgs(ambiente, argc, argv);

  // Abre o arquivo
  ambiente->arqIn = fopen(ambiente->fileNameIn, "r");
  if (!ambiente->arqIn) {
    printf("Nao foi possivel abrir o arquivo '%s'!\n", ambiente->fileNameIn);
    exit(EXIT_FAILURE);
  }

  string_t linhaAtual = NULL;
  comando_t *comando  = NULL;

  while (lerLinha(&linhaAtual, ambiente->arqIn)) {
    // Aumenta a linha atual do ambiente
    ambiente->linhaAtual++;

    // Ler as operações da linha atual e salva na variável comando
    comando = lerComando(linhaAtual);

    // Transforma referencias as variaveis e parametros nas suas strings
    evaluateVars(ambiente, comando);

    // Executa os comandos
    executarComando(ambiente, comando);

    // Free no comando
    if (comando->params[0])
      free(comando->params[0]);
    if (comando->params[1])
      free(comando->params[1]);
    free(comando);
    comando = NULL;
  }

  fclose(ambiente->arqIn);

  printf("Arquivo \"%s\" finalizado.\n", ambiente->fileNameIn);

  // Limpa tudo
  free(linhaAtual);
  freeAmbiente(&ambiente);

  return 0;
}